package com.angad.mindvalley;

/**
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class AppConstant {

    public static final String APP_NAME = "Pin - MINDVALLEY";
    public static final String APP_AUTHOR = "Angad Tiwari";
    public static final String APP_AUTHOR_WITH_PREFIX = "@Author Angad Tiwari";

    public static final String DATA_API_URL = "http://pastebin.com";

    public static final long MAX_MEMORY_LIMIT = Runtime.getRuntime().maxMemory()/4;

    /**
     * API_TYPE (means JSON Based API or XML Based API)
     * Default is set as JSON_BASED_API
     * you can change acc. to your API_TYPE
     */
    public static final int JSON_BASED_API = 1;
    public static final int XML_BASED_API = 2;
    public static final int API_TYPE = JSON_BASED_API; //change this to above any API_TYPES

    /**
     * Splash Activity Logo Animation Constants
     */
    public static final float ALPHA_ANIMATION_FROM = 1.0f;
    public static final float ALPHA_ANIMATION_TO = 0.2f;
    public static final int ALPHA_ANIMATION_DURATION = 600;
    public static final int ALPHA_ANIMATION_REPEAT_COUNT = 5;

    public static final float MOVE_ANIMATION_FROM_XDELTA = 0.0f;
    public static final float MOVE_ANIMATION_TO_XDELTA = 0.0f;
    public static final float MOVE_ANIMATION_FROM_YDELTA = -100.0f;
    public static final float MOVE_ANIMATION_TO_YDELTA = 0.0f;
    public static final int MOVE_ANIMATION_DURATION = 1000;
    public static final int MOVE_ANIMATION_REPEAT_COUNT = 3;

    /**
     * Dialog Contants
     */
    public static final String DEFAULT_DIALOG_MESSAGE = "Dialog message !!";
    public static final String POSITIVE_BUTTON_TEXT = "Ok";
    public static final String NEGATIVE_BUTTON_TEXT = "Cancel";
}
