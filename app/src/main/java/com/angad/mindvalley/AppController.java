package com.angad.mindvalley;

import android.app.Application;

import com.angad.mindvalley.network.AppAPIEndpoint;

import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * AppController is the Base class for maintaining global application state
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class AppController extends Application {

    private Retrofit retrofit;
    private static AppController instance;
    private AppAPIEndpoint appAPIEndpoint;
    private String TAG = AppController.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        retrofit = new Retrofit.Builder()
                .baseUrl(AppConstant.DATA_API_URL)
                .addConverterFactory(chooseConverterFactoryMethod())
                .build();
        appAPIEndpoint = retrofit.create(AppAPIEndpoint.class);
    }

    /**
     * block the constuctor using private keyboard
     * Singleton Design Pattern
     */
    //private AppController() {}

    public static synchronized AppController getInstance(){
        if(instance==null)
            instance = new AppController();

        return instance;
    }

    public AppAPIEndpoint getAppAPIEndpoint(){
        return appAPIEndpoint;
    }

    /**
     * Choose the appropriate Converter Factory Class for Retrofit2 (Networking Library)
     * @return return the value from AppConstant.API_TYPE and if no value is provided then default value is AppConstant.JSON_BASED_API (i.e. 1)
     */
    private Converter.Factory chooseConverterFactoryMethod(){

        Converter.Factory retrofitConverterFactory ;
        switch (AppConstant.API_TYPE){
            case AppConstant.JSON_BASED_API:{
                retrofitConverterFactory = GsonConverterFactory.create();
                break;
            }
            case AppConstant.XML_BASED_API:{
                retrofitConverterFactory = SimpleXmlConverterFactory.create();
                break;
            }
            default: {
                retrofitConverterFactory = GsonConverterFactory.create();
                break;
            }
        }

        return retrofitConverterFactory;
    }
}
