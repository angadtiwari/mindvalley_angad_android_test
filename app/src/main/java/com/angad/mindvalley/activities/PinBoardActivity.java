package com.angad.mindvalley.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.angad.mindvalley.AppController;
import com.angad.mindvalley.R;
import com.angad.mindvalley.adapters.PinboardAdapter;
import com.angad.mindvalley.models.AppDataModel;
import com.angad.mindvalley.utilities.AppDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Pinboard Activity contain the Pins
 * SwipeRefreshLayout is been used to refresh the whole pinboard
 * StaggeredGridLayoutManager is an enhance version of GridLayoutManger where the items is been WRAP_CONTENT
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class PinBoardActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView mPinboardRecylcer;
    private SwipeRefreshLayout mPinboardSwipeRefreshLayout;
    private StaggeredGridLayoutManager gridLayoutManager;
    private PinboardAdapter pinboardAdapter;
    private List<AppDataModel> pins = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_board);

        setupToolbar();
        setupView();
        fetchData();

        mPinboardSwipeRefreshLayout.setOnRefreshListener(this);
    }

    /**
     * this method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        fetchData();
    }

    /**
     * fetchData method consume the API which return the response (also the data for our application)
     */
    private void fetchData() {
        Call<List<AppDataModel>> appDataModelCall = AppController.getInstance().getAppAPIEndpoint().getAppData();
        appDataModelCall.enqueue(new Callback<List<AppDataModel>>() {
            @Override
            public void onResponse(Call<List<AppDataModel>> call, Response<List<AppDataModel>> response) {
                pins.clear();
                pins.addAll(response.body());
                pinboardAdapter.notifyDataSetChanged();
                // stopping swipe refresh
                mPinboardSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<AppDataModel>> call, Throwable t) {
                t.printStackTrace();
                // stopping swipe refresh
                mPinboardSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * setupView method initialize the view and map to object from layout
     */
    private void setupView() {

        mPinboardSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.pinboard_swiperefreshlayout);
        mPinboardRecylcer = (RecyclerView) findViewById(R.id.pinboard_recyclerview);
        gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mPinboardRecylcer.setLayoutManager(gridLayoutManager);
        pinboardAdapter = new PinboardAdapter(this, pins);
        mPinboardRecylcer.setAdapter(pinboardAdapter);
    }

    /**
     * inflate the menu from menu_pinboard which contain info & profile icon
     * @param menu menu at which inflation happen
     * @return is the inflation consumed or not
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pinboard, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * selection event on option menu both on info & profile icon
     * @param item contain the object of selected menu-item
     * @return is the selection is consumed or not
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.id_menu_info: {
                new AppDialog.AppDialogBuilder(PinBoardActivity.this)
                        .setViewId(R.layout.dialog_app_info)
                        .setCancelable(true)
                        .build();
                break;
            }
            case R.id.id_menu_profile: {
                Intent pinboardIntent = new Intent(PinBoardActivity.this, ProfileActivity.class);
                startActivity(pinboardIntent);
                overridePendingTransition(R.anim.slidein_animation, R.anim.zoomout_animation);
                break;
            }
            default: {
                break;
            }
        }

        return true;
    }

    /**
     * setupToolbar method inflate the toolbar instead of actionbar
     * also set false to displayShowTitleEnable which disable the Activity's Label to set in the Title area of toolbar
     */
    private void setupToolbar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
}
