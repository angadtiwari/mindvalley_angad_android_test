package com.angad.mindvalley.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.angad.mindvalley.AppConstant;
import com.angad.mindvalley.R;
import com.angad.mindvalley.utilities.AppDialog;

/**
 * Profile Screen of the application
 * Contain FloatingButton for the ProfileRating.
 * Also this FloatingButton with profile image is been collapsed after collapsing the appbar
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    private FloatingActionButton fab_rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setupToolbar();
        setupViews();

        fab_rating.setOnClickListener(this);
    }

    /**
     * listen to click event
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            /**
             * this will shows the dialog with current profile rating
             * AppDialog is designed with BUILDER-DESIGN-PATTERN where AppDialogBuilder is the Builder class
             */
            case R.id.fab_rating: {
                new AppDialog.AppDialogBuilder(ProfileActivity.this)
                        .setTitle(AppConstant.APP_NAME)
                        .setMessage("+1 is your Profile Rating")
                        .setCancelable(true)
                        .build();
                break;
            }
        }
    }

    /**
     * setup the views for the profile screen
     */
    private void setupViews(){

        fab_rating = (FloatingActionButton) findViewById(R.id.fab_rating);
    }

    /**
     * setup the toolbar for the profile scren , also set the title with the profile-name and set back button enable
     */
    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle(getResources().getString(R.string.app_author));
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.toolbar_back_icon);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * listen to the back button pressed
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home: {
                onBackPressed();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * override the backpressed to attach custom animation
     */
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.zoomin_animation,R.anim.slideout_animation);
    }
}
