package com.angad.mindvalley.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.angad.mindvalley.R;
import com.angad.mindvalley.animations.BlinkAnimation;
import com.angad.mindvalley.animations.MoveAnimation;

/**
 * MAIN Activity for the Application
 * Contain the Logo with animation, Currently MoveAnimation (Bounce Animation) is Active
 * Although you can also add/substitute your own Animation by implementing the IAppAnimator interface and define our own animation
 * override method of IAppAnimator is animation() which contains the params as <view>view to animate</view> & and <animationlisterner> animation listener </animationlisterner>
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener{

    private ImageView imageview_logo;
    private Handler handler_redirector;
    private Runnable runnable_redirector;

    private String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageview_logo = (ImageView) findViewById(R.id.logo);
        new MoveAnimation().animation(imageview_logo, this);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // if you would like to do any thing than do it
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        handler_redirector = new Handler();
        handler_redirector.postDelayed(runnable_redirector = new Runnable() {
            @Override
            public void run() {

                Intent pinboardIntent = new Intent(SplashActivity.this, PinBoardActivity.class);
                startActivity(pinboardIntent);
                finish();
                overridePendingTransition(R.anim.slidein_animation, R.anim.zoomout_animation);
            }
        },600);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // if you would like to do any thing than do it
    }
}
