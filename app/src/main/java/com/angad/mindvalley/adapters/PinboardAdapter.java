package com.angad.mindvalley.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.angad.appresourceloader.ResourceLoader;
import com.angad.mindvalley.AppConstant;
import com.angad.mindvalley.AppController;
import com.angad.mindvalley.R;
import com.angad.mindvalley.models.AppDataModel;
import com.angad.mindvalley.utilities.MaxMemoryCalculator;

import java.util.List;

/**
 * PinboardAdapter is the Adapter for Pins in the PinboardScreen
 * DEFAULT_PIN_BG_COLORS is the int array for default color for pin background, which shows until the image is loaded
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 23-July-2016
 */
public class PinboardAdapter extends RecyclerView.Adapter<PinboardAdapter.ViewHolder> {

    private Context context;
    private List<AppDataModel> pins;
    private ResourceLoader imageLoader;

    private int[] DEFAULT_PIN_BG_COLORS = new int[] {Color.parseColor("#FFCCD2"), Color.parseColor("#F8BBD0"), Color.parseColor("#E1BEE7"),
    Color.parseColor("#C5CAE9"), Color.parseColor("#B2DFDB"), Color.parseColor("#DCEDC8"), Color.parseColor("#FFF9C4"),
    Color.parseColor("#FFE0B2"), Color.parseColor("#FFCCBC"), Color.parseColor("#D7CCC8")};

    public PinboardAdapter(Context context, List<AppDataModel> pins){
        this.context = context;
        this.pins = pins;
        this.imageLoader = new ResourceLoader(context, MaxMemoryCalculator.calculateMaxMemoryCacheSize(context));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_pin, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        setupDynamicHeightOfPin(holder, position);

        /*Picasso.with(context)
                .load(pins.get(position).getUrls().getFull())
                .placeholder(new ColorDrawable(DEFAULT_PIN_BG_COLORS[position % DEFAULT_PIN_BG_COLORS.length]))
                .into(holder.image_pin);*/
        imageLoader.displayImage(pins.get(position).getUrls().getFull(), holder.image_pin, new ColorDrawable(DEFAULT_PIN_BG_COLORS[position % DEFAULT_PIN_BG_COLORS.length]));
    }

    /**
     * this method set the height of each view from pins.get(position).getHeight(), which returns by API
     *
     * @param holder object of the ViewHolder clas
     * @param position the position at which this needs to apply
     */
    private void setupDynamicHeightOfPin(ViewHolder holder, int position) {

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)holder.image_pin.getLayoutParams();
        layoutParams.height=pins.get(position).getHeight();

        holder.image_pin.setLayoutParams(layoutParams);
        holder.image_pin_overlay.setLayoutParams(layoutParams);
    }

    /**
     * override the adapter count function, also handle the null for pins object
     * @return returns the no. of items for the adapter
     */
    @Override
    public int getItemCount() {
        return pins==null ? 0: pins.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image_pin, image_pin_overlay;

        public ViewHolder(View view){
            super(view);

            image_pin = (ImageView) view.findViewById(R.id.image_pin);
            image_pin_overlay = (ImageView) view.findViewById(R.id.image_pin_overlay);
        }
    }
}
