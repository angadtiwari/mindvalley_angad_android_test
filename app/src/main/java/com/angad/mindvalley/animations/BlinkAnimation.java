package com.angad.mindvalley.animations;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;

import com.angad.mindvalley.AppConstant;
import com.angad.mindvalley.interfaces.IAppAnimator;

/**
 * Custom Animation which blinks the view and return the callback to animationListener
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class BlinkAnimation implements IAppAnimator {

    @Override
    public void animation(View view, Animation.AnimationListener animationListener) {

        AlphaAnimation blinkanimation= new AlphaAnimation(AppConstant.ALPHA_ANIMATION_FROM, AppConstant.ALPHA_ANIMATION_TO); // Change alpha
        blinkanimation.setDuration(AppConstant.ALPHA_ANIMATION_DURATION); // set duration
        blinkanimation.setInterpolator(new LinearInterpolator()); // set the interpolator linear,swing etc
        blinkanimation.setRepeatCount(AppConstant.ALPHA_ANIMATION_REPEAT_COUNT); // Repeat animation accordingly
        blinkanimation.setRepeatMode(Animation.REVERSE); // Reverse the animation after each successive
        view.setAnimation(blinkanimation);
        blinkanimation.setAnimationListener(animationListener);
    }
}
