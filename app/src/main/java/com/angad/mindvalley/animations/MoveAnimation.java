package com.angad.mindvalley.animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;

import com.angad.mindvalley.AppConstant;
import com.angad.mindvalley.interfaces.IAppAnimator;

/**
 * Custom Animation which blinks the view and return the callback to animationListener
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class MoveAnimation implements IAppAnimator {

    @Override
    public void animation(View view, Animation.AnimationListener animationListener) {

        TranslateAnimation translateAnimation= new TranslateAnimation(AppConstant.MOVE_ANIMATION_FROM_XDELTA,
                AppConstant.MOVE_ANIMATION_TO_XDELTA,
                AppConstant.MOVE_ANIMATION_FROM_YDELTA,
                AppConstant.MOVE_ANIMATION_TO_YDELTA); // Change X,Y Delta
        translateAnimation.setDuration(AppConstant.MOVE_ANIMATION_DURATION); // set duration
        translateAnimation.setInterpolator(new BounceInterpolator()); // set the interpolator linear,swing etc
        translateAnimation.setRepeatCount(AppConstant.MOVE_ANIMATION_REPEAT_COUNT); // Repeat animation accordingly
        translateAnimation.setRepeatMode(Animation.REVERSE); // Reverse the animation after each successive
        view.setAnimation(translateAnimation);
        translateAnimation.setAnimationListener(animationListener);
    }
}