package com.angad.mindvalley.interfaces;

import android.view.View;
import android.view.animation.Animation;

/**
 * IAppAnimator interface contain abstract method animation()
 * animation() contains params as View (at which animation to be applied) & AnimationListener (callback for animation such as onanimationstart, onanimationrepeat, onanimationend)
 * implement this interface and write your own Animation class
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public interface IAppAnimator {
    void animation(View view, Animation.AnimationListener animationListener);
}