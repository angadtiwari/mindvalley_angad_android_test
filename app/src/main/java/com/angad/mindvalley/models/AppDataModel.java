package com.angad.mindvalley.models;

import java.util.List;

/**
 * this Model is generated using GsonFormat Plugin Version 1.2.3 and sample data is response of API Call (in json)
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class AppDataModel {

        /**
         * id : 4kQA1aQK8-Y
         * created_at : 2016-05-29T15:42:02-04:00
         * width : 2448
         * height : 1836
         * color : #060607
         * likes : 12
         * liked_by_user : false
         * user : {"id":"OevW4fja2No","username":"nicholaskampouris","name":"Nicholas Kampouris","profile_image":{"small":"","medium":"","large":""},"links":{"self":"","html":"","photos":"","likes":""}}
         * current_user_collections : []
         * urls : {"raw":"","full":"","regular":"","small":"","thumb":""}
         * categories : [{"id":4,"title":"Nature","photo_count":46148,"links":{"self":"","photos":""}},{"id":6,"title":"People","photo_count":15513,"links":{"self":"","photos":""}}]
         * links : {"self":"","html":"","download":""}
         */

        private String id;
        private String created_at;
        private int width;
        private int height;
        private String color;
        private int likes;
        private boolean liked_by_user;
        /**
         * id : OevW4fja2No
         * username : nicholaskampouris
         * name : Nicholas Kampouris
         * profile_image : {"small":"","medium":"","large":""}
         * links : {"self":"","html":"","photos":"","likes":""}
         */

        private UserBean user;
        /**
         * raw :
         * full :
         * regular :
         * small :
         * thumb :
         */

        private UrlsBean urls;
        /**
         * self :
         * html :
         * download :
         */

        private LinksBean links;
        private List<?> current_user_collections;
        /**
         * id : 4
         * title : Nature
         * photo_count : 46148
         * links : {"self":"","photos":""}
         */

        private List<CategoriesBean> categories;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }

        public boolean isLiked_by_user() {
            return liked_by_user;
        }

        public void setLiked_by_user(boolean liked_by_user) {
            this.liked_by_user = liked_by_user;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public UrlsBean getUrls() {
            return urls;
        }

        public void setUrls(UrlsBean urls) {
            this.urls = urls;
        }

        public LinksBean getLinks() {
            return links;
        }

        public void setLinks(LinksBean links) {
            this.links = links;
        }

        public List<?> getCurrent_user_collections() {
            return current_user_collections;
        }

        public void setCurrent_user_collections(List<?> current_user_collections) {
            this.current_user_collections = current_user_collections;
        }

        public List<CategoriesBean> getCategories() {
            return categories;
        }

        public void setCategories(List<CategoriesBean> categories) {
            this.categories = categories;
        }

        public static class UserBean {
            private String id;
            private String username;
            private String name;
            /**
             * small :
             * medium :
             * large :
             */

            private ProfileImageBean profile_image;
            /**
             * self :
             * html :
             * photos :
             * likes :
             */

            private LinksBean links;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public ProfileImageBean getProfile_image() {
                return profile_image;
            }

            public void setProfile_image(ProfileImageBean profile_image) {
                this.profile_image = profile_image;
            }

            public LinksBean getLinks() {
                return links;
            }

            public void setLinks(LinksBean links) {
                this.links = links;
            }

            public static class ProfileImageBean {
                private String small;
                private String medium;
                private String large;

                public String getSmall() {
                    return small;
                }

                public void setSmall(String small) {
                    this.small = small;
                }

                public String getMedium() {
                    return medium;
                }

                public void setMedium(String medium) {
                    this.medium = medium;
                }

                public String getLarge() {
                    return large;
                }

                public void setLarge(String large) {
                    this.large = large;
                }
            }

            public static class LinksBean {
                private String self;
                private String html;
                private String photos;
                private String likes;

                public String getSelf() {
                    return self;
                }

                public void setSelf(String self) {
                    this.self = self;
                }

                public String getHtml() {
                    return html;
                }

                public void setHtml(String html) {
                    this.html = html;
                }

                public String getPhotos() {
                    return photos;
                }

                public void setPhotos(String photos) {
                    this.photos = photos;
                }

                public String getLikes() {
                    return likes;
                }

                public void setLikes(String likes) {
                    this.likes = likes;
                }
            }
        }

        public static class UrlsBean {
            private String raw;
            private String full;
            private String regular;
            private String small;
            private String thumb;

            public String getRaw() {
                return raw;
            }

            public void setRaw(String raw) {
                this.raw = raw;
            }

            public String getFull() {
                return full;
            }

            public void setFull(String full) {
                this.full = full;
            }

            public String getRegular() {
                return regular;
            }

            public void setRegular(String regular) {
                this.regular = regular;
            }

            public String getSmall() {
                return small;
            }

            public void setSmall(String small) {
                this.small = small;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }
        }

        public static class LinksBean {
            private String self;
            private String html;
            private String download;

            public String getSelf() {
                return self;
            }

            public void setSelf(String self) {
                this.self = self;
            }

            public String getHtml() {
                return html;
            }

            public void setHtml(String html) {
                this.html = html;
            }

            public String getDownload() {
                return download;
            }

            public void setDownload(String download) {
                this.download = download;
            }
        }

        public static class CategoriesBean {
            private int id;
            private String title;
            private int photo_count;
            /**
             * self :
             * photos :
             */

            private LinksBean links;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getPhoto_count() {
                return photo_count;
            }

            public void setPhoto_count(int photo_count) {
                this.photo_count = photo_count;
            }

            public LinksBean getLinks() {
                return links;
            }

            public void setLinks(LinksBean links) {
                this.links = links;
            }

            public static class LinksBean {
                private String self;
                private String photos;

                public String getSelf() {
                    return self;
                }

                public void setSelf(String self) {
                    this.self = self;
                }

                public String getPhotos() {
                    return photos;
                }

                public void setPhotos(String photos) {
                    this.photos = photos;
                }
            }
        }
}
