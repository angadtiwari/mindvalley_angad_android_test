package com.angad.mindvalley.network;

import com.angad.mindvalley.models.AppDataModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public interface AppAPIEndpoint {

    /**
     * Fetch the Data for the Application via <DATA_API_URL/raw/wgkJgazE>
     * @return Serialized Object which is converted using appropriate Converter-Factory-Method choosen in AppController.chooseConverterFactoryMethod()
     */
    @GET("/raw/wgkJgazE")
    Call<List<AppDataModel>> getAppData();
}
