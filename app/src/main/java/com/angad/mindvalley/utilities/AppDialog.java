package com.angad.mindvalley.utilities;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.angad.mindvalley.AppConstant;
import com.angad.mindvalley.R;

/**
 * AlertDialog preparer for the app
 * Based on Builder-Design-Pattern
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class AppDialog {

    public AppDialog(AppDialogBuilder appDialogBuilder){

        String title = appDialogBuilder.title==null ? AppConstant.APP_NAME : appDialogBuilder.title;
        String message = appDialogBuilder.message==null ? AppConstant.DEFAULT_DIALOG_MESSAGE : appDialogBuilder.message;
        String positiveButtonText = appDialogBuilder.positiveButtonText==null ? AppConstant.POSITIVE_BUTTON_TEXT : appDialogBuilder.positiveButtonText;
        String negativeButtonText = appDialogBuilder.negativeButtonText==null ? AppConstant.NEGATIVE_BUTTON_TEXT : appDialogBuilder.negativeButtonText;

        AlertDialog.Builder builder =
                new AlertDialog.Builder(appDialogBuilder.context, R.style.AppThemeDialog);

        if(appDialogBuilder.viewId!=0) {
            builder.setView(appDialogBuilder.viewId);
        }
        else {
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setCancelable(appDialogBuilder.cancelable);

            if (appDialogBuilder.showPositiveButton) {
                builder.setPositiveButton(positiveButtonText, appDialogBuilder.positiveButtonListener);
            }
            if (appDialogBuilder.showNegativeButton) {
                builder.setNegativeButton(negativeButtonText, appDialogBuilder.negativeButtonListener);
            }
        }
        builder.show();
    }

    /**
     * Builder class for the AppDialog Class
     */
    public static class AppDialogBuilder {

        private Context context;
        private int viewId;
        private String title;
        private String message;
        private boolean cancelable;
        private boolean showPositiveButton;
        private boolean showNegativeButton;
        private String positiveButtonText;
        private String negativeButtonText;
        private DialogInterface.OnClickListener positiveButtonListener;
        private DialogInterface.OnClickListener negativeButtonListener;

        public AppDialogBuilder(Context context){
            this.context = context;
        }

        public AppDialogBuilder setTitle(String title) {
            this.title = title;

            return this;
        }

        public AppDialogBuilder setViewId(int viewId) {
            this.viewId = viewId;

            return this;
        }

        public AppDialogBuilder setMessage(String message) {
            this.message = message;

            return this;
        }

        public AppDialogBuilder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;

            return this;
        }

        public AppDialogBuilder setShowPositiveButton(boolean positiveButton) {
            this.showPositiveButton = positiveButton;

            return this;
        }

        public AppDialogBuilder setSetNegativeButton(boolean negativeButton) {
            this.showNegativeButton = negativeButton;

            return this;
        }

        public AppDialogBuilder setPositiveButtonText(String positiveButtonText) {
            this.positiveButtonText = positiveButtonText;

            return this;
        }

        public AppDialogBuilder setNegativeButtonText(String negativeButtonText) {
            this.negativeButtonText = negativeButtonText;

            return this;
        }

        public AppDialogBuilder setPositiveButtonListener(DialogInterface.OnClickListener positiveButtonListener) {
            this.positiveButtonListener = positiveButtonListener;

            return this;
        }

        public AppDialogBuilder setNegativeButtonListener(DialogInterface.OnClickListener negativeButtonListener) {
            this.negativeButtonListener = negativeButtonListener;

            return this;
        }

        public AppDialog build() {
            return new AppDialog(this);
        }
    }
}
