package com.angad.mindvalley.utilities;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;

/**
 * Calculate the maximum memory for caching
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class MaxMemoryCalculator {

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static class ActivityManagerHoneycomb {
        static int getLargeMemoryClass(ActivityManager activityManager) {
            return activityManager.getLargeMemoryClass();
        }
    }

    @SuppressWarnings("unchecked")
    static <T> T getService(Context context, String service) {
        return (T) context.getSystemService(service);
    }

    /**
     * calculate the maximum memory cache size
     * @return max memory cache size
     */
    public static long calculateMaxMemoryCacheSize(Context context){

        ActivityManager am = getService(context, context.ACTIVITY_SERVICE);
        boolean largeHeap = (context.getApplicationInfo().flags & ApplicationInfo.FLAG_LARGE_HEAP) != 0;
        int memoryClass = am.getMemoryClass();
        if (largeHeap && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            memoryClass = ActivityManagerHoneycomb.getLargeMemoryClass(am);
        }
        // Target ~15% of the available heap.
        return (int) (1024L * 1024L * memoryClass / 7);
    }
}
