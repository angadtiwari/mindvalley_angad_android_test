package com.angad.appresourceloader;

import java.io.File;
import android.content.Context;

/**
 * FileCache class to cache the file
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public class FileCache {

    private File cacheDir;

    public FileCache(Context context){
        //Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(), "ImageCache");
        else
            cacheDir=context.getCacheDir();
        if(!cacheDir.exists())
            cacheDir.mkdirs();
    }

    public File getFile(String url){
        //Identify images by hashcode
        String filename=String.valueOf(url.hashCode());
        //Another possible solution
        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;

    }

    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }

}