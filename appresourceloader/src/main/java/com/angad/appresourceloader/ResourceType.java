package com.angad.appresourceloader;

/**
 * Defines the resource type which needs to download
 * i.e. image or json (can enhance the enum for various data-type)
 *
 * @author Angad Tiwari
 * @version 1.0
 * @since 21-July-2016
 */
public enum ResourceType {

    IMAGE("image/jpg"), JSON("application/json");

    private final String content_type;

    private ResourceType(String s) {
        content_type = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : content_type.equals(otherName);
    }

    public String toString() {
        return this.content_type;
    }
}
